package com.gmail.val59000mc.scenarios.scenariolisteners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.events.PlayerStartsPlayingEvent;
import com.gmail.val59000mc.exceptions.UhcPlayerNotOnlineException;
import com.gmail.val59000mc.players.UhcPlayer;
import com.gmail.val59000mc.scenarios.ScenarioListener;

public class CatEyesListener extends ScenarioListener {
    
    @EventHandler
    public void onGameStart(PlayerStartsPlayingEvent e) {
        UhcPlayer uhcplayer = e.getUhcPlayer();
        Player player;

        try {
            player = uhcplayer.getPlayer();
        } catch (UhcPlayerNotOnlineException ex) {
            // No effect for offline player
            return;
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999, 0));
    }

}
