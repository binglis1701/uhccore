package com.gmail.val59000mc.scenarios.scenariolisteners;

import com.gmail.val59000mc.events.UhcStartedEvent;
import com.gmail.val59000mc.languages.Lang;
import com.gmail.val59000mc.exceptions.UhcPlayerNotOnlineException;
import com.gmail.val59000mc.players.UhcPlayer;
import com.gmail.val59000mc.scenarios.ScenarioListener;
import com.gmail.val59000mc.utils.UniversalMaterial;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.Material;

public class BowFightersListener extends ScenarioListener {

	@EventHandler
	public void onGameStarted(UhcStartedEvent e){

        // enchanted infinity book
        ItemStack infinity_book = new ItemStack(UniversalMaterial.ENCHANTED_BOOK.getStack());
        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) infinity_book.getItemMeta();
        meta.addStoredEnchant(Enchantment.ARROW_INFINITE, 1, true);
        infinity_book.setItemMeta(meta);

		for (UhcPlayer uhcPlayer : e.getPlayerManager().getOnlinePlayingPlayers()){
			try {
				// Give the items
				uhcPlayer.getPlayer().getInventory().addItem(UniversalMaterial.STRING.getStack(2));
                uhcPlayer.getPlayer().getInventory().addItem(UniversalMaterial.ARROW.getStack(4));
                uhcPlayer.getPlayer().getInventory().addItem(infinity_book);
			}catch (UhcPlayerNotOnlineException ex){
				// No items for offline players
			}
		}
	}

    @EventHandler
	public void onCraftItem(CraftItemEvent e) {
		ItemStack item = e.getCurrentItem();
        // strogn tools e.g. gold, iron and diamond sword and axe.
		if (item.getType().equals(UniversalMaterial.GOLDEN_SWORD.getType()) || item.getType().equals(UniversalMaterial.GOLDEN_AXE.getType()) ||
            item.getType().equals(UniversalMaterial.IRON_SWORD.getType()) || item.getType().equals(UniversalMaterial.IRON_AXE.getType()) ||
            item.getType().equals(UniversalMaterial.DIAMOND_SWORD.getType()) || item.getType().equals(UniversalMaterial.DIAMOND_AXE.getType())) {
			e.getWhoClicked().sendMessage(Lang.SCENARIO_BOW_FIGHTERS_ERROR);
			e.setCancelled(true);
		}
	}

}
